TEXS             := $(wildcard tex/*/_*.tex)
SVGS             := $(wildcard img/*.svg)
TMP              := .tmp

## Target if make is invoked without any parameter (goal)
.DEFAULT_GOAL: all

## "Virtual" targets without actual files to update/create
.PHONY: all clean distclean new


all: $(TEXS:tex/%.tex=dist/%.pdf)

FORCE:
	@true

.ONESHELL:
$(TMP)/tex/%.pdf: FORCE $(SVGS:img/%.svg=.tmp/img/%.pdf)
	@FILE="$$(basename "$@" .pdf)"
	export DIR="$$(dirname "$(@:.tmp/tex/%=%)")"
	export max_print_line=1000
	export error_line=254
	export half_error_line=238
	
	echo -e "\033[48;5;237m Making: $$DIR / $$FILE.tex \033[K\033[0m\n"
	
	mkdir -p ".tmp/tex/$$DIR"
	
	if ! (cd "tex/$$DIR" && latexmk "$$FILE.tex") ; then
		grep  -H -C 10 --color=always -n -E '^(\./|\!).*' .tmp/tex/$$DIR/$$FILE.log | \
		python3 -c 'import sys;print(sys.stdin.read().replace(sys.argv[1], sys.argv[2]))' .tmp/tex/$$DIR/$$FILE.log "" | \
		python3 -c 'import sys;print(sys.stdin.read().replace(sys.argv[1], sys.argv[2]))' $$'\033[K./' "$$PWD/"
		
		rm -f "$@"
		false
	fi


$(TMP)/img/%.pdf: img/%.svg
	mkdir -p .tmp/img
	inkscape --export-text-to-path --export-area-drawing --export-pdf $@ $^

dist/%.pdf: .tmp/tex/%.pdf
	mkdir -p $(@D)
	ln --force $^ $@

clean:
	rm --force --verbose --recursive .tmp

distclean: clean
	rm --force --verbose --recursive dist

new: distclean
	$(MAKE)
