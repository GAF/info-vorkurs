# Info Vorkurs
HTML/Linux/Python preliminary course materials.
Didactic concepts are documented in the [Wiki](https://gaf.fs.lmu.de/wiki/Vorkurs_Info).

## Istallation
### Requirements
* [Python 3](https://www.python.org/)
* [TeX Live](https://www.tug.org/texlive/) (with latexmk and XeTeX)
* [Make](https://www.gnu.org/software/make/)
* [Inkscape](https://inkscape.org/)

To download the corporate design (which is a submodule), you also need access the [O-Phase/cd](https://git.fs.lmu.de/O-Phase/cd) repository.

### Get the source
This project uses submodules, therefore you should clone with
```sh
git clone --recursive git@git.fs.lmu.de:gaf/info-vorkurs.git
```

### Build
From the project root directory, you can invoke the following build commands:
`make` - Build everything that has changed (does not alywas work, use `distclean` before to fix) <br />
`make clean` - Remove all intermediate files in `.tmp` <br />
`make distclean` - Remove all output files in `dist` and all intermediate files <br />
`make new` - Rebuild everything from ground up (combination of `make` and `make distclean`) <br />
`make <file>` - Build a specific file


## Structure
* `/img/` - Images, which can be used by LaTeX.
* `/tex/<project>/` - Directory of a LaTeX Project (e.g. slides).
* `/tex/<project>/_*.tex` - LaTeX files, which will be build. Symbolic links can be used to add an additional job name for the same source (e.g. to create an annotated version of slides).
* `/tex/latexmkrc` - latexmk configuration file used by all projects, who add a symbolic link `.latexmkrc` to this file in their LaTeX project directory.
* `/.tmp/` - Automatically created files. This directory can safely be removed.
* `/.tmp/img/` - SVG files converted to PDF will be saved here.
* `/.tmp/tex/<project>/_*.pdf` - Compiled LaTeX files.
* `/dist/` - The final PDFs will be saved here.
